---
title: GJS Guides
date: 2018-07-25 16:10:11
layout: IndexPage
---

# Developer Guides

    Welcome to GJS!

<ShowCaseBox title="GTK+" subtitle="Looking for help building an application?">
<ShowCase link="gtk/gtk-tutorial/" title="Getting Started with GTK+" subtitle="New to GTK+ or using it with GJS? This is for you!" />
<ShowCase link="gtk/application-packaging.html" title="GTK+ Application Packaging" subtitle="How do I package my GTK+ application with GJS?"  />
</ShowCaseBox>

<ShowCaseBox title="GJS" subtitle="What sets GJS apart from other JavaScript environments?">
<ShowCase link="gjs/style-guide.html" title="Style Guide" subtitle="The official style guide for GJS and GNOME projects written in GJS."  />
<ShowCase link="gjs/transition.html" title="Transition" subtitle="How does GJS compare to other JavaScript environments?"  />
<ShowCase link="gjs/legacy-class-syntax.html" title="Legacy Classes" subtitle="How do I use the deprecated Lang.Class objects?"  />
<ShowCase link="gjs/features-across-versions.html" title="Feature Compatibility" subtitle="Which features work in my version of GJS?"  />
</ShowCaseBox>



<ShowCaseBox title="Gio" subtitle="Looking for help with files or application settings?">
<ShowCase link="gio/file-operations.html" title="Files in GJS" subtitle="Basic File Operations in GJS"  />
</ShowCaseBox>