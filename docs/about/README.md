---
title: About
---

# About GJS

## What is GJS?

GJS (sometimes called GNOME JavaScript) is a programming environment comparable to Node.js. It provides bindings to the [GNOME](https://gnome.org) ecosystem, allowing users to build power, native applications.

## Cool! What is under the hood?

GJS is built on the [SpiderMonkey](https://developer.mozilla.org/en-US/docs/Mozilla/Projects/SpiderMonkey) JavaScript engine. It utilizes [GObject Introspection]() to provide access to the GNOME APIs using EcmaScript 6-level JavaScript.