---
title: Examples
date: 2018-07-25 16:10:11
---

# Examples

## Follow along as we create some amazing GNOME apps in GJS!

<ShowCase link="hello/" title="Hello GNOME!" subtitle="Learn how to build the age-old 'Hello World' app in GJS!" image="" />

<ShowCase link="tags/" title="Tags" subtitle="Learn how to build a tags and notes app for your files!" image="" />

<ShowCase link="tictactoe/" title="TicTacToe" subtitle="Learn how to build the classic fun game!" image="" />
